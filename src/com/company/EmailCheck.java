package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailCheck {

    public static boolean validateEmail(String email) {
        String ePattern = "^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void main(String[] args) {

        System.out.println(validateEmail("olga@gmail.com"));
    }
}
